package main

import (
	"errors"
	"fmt"
	"os"

	"gitlab.com/sonicrainboom/laser/backends/cdinfo"
	"gitlab.com/sonicrainboom/laser/backends/cdparanoia"
	"gitlab.com/sonicrainboom/laser/jsonout"
)

func main() {
	status := jsonout.StatusObject{}

	defer func() {
		if err := recover(); err != nil {
			switch err.(type) {
			case error:
				jsonout.WriteError(err.(error))
			default:
				jsonout.WriteError(errors.New(fmt.Sprintf("%+v", err)))
			}
			if status.Workdir != ""  && status.RemoveDir {
				_ = os.RemoveAll(status.Workdir)
			}
			os.Exit(1)
		}
		if status.Workdir != "" && status.RemoveDir {
			_ = os.RemoveAll(status.Workdir)
		}
		os.Exit(0)
	}()
	dir, err := os.MkdirTemp(os.Getenv("SRB_LASER_TMP"), "laser_*")
	if err != nil {
		panic(err)
	}
	status.Workdir = dir

	if len(os.Args) <= 1 {
		panic("no args")
	}

	switch os.Args[1] {
	case "analyze":
		status.WorkType = "analyze"
		jsonout.WriteStatus(status)
		cdparanoia.AnalyzeDrive(status, "/dev/cdrom")
	case "info":
		status.WorkType = "info"
		jsonout.WriteStatus(status)
		cdinfo.DiscInfo(&status, "/dev/cdrom", true)
	case "rip":
		status.WorkType = "rip"
		jsonout.WriteStatus(status)
		cdinfo.DiscInfo(&status, "/dev/cdrom", false)
		cdparanoia.RipDisc(&status, "/dev/cdrom")
	default:
		panic("unknown command")
	}
}
