package cdparanoia

import (
	"bufio"
	"fmt"
	"gitlab.com/sonicrainboom/laser/jsonout"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"regexp"
	"strconv"
)

func AnalyzeDrive(status jsonout.StatusObject, drive string) {
	status.RemoveDir = false

	f, err := ioutil.TempFile(status.Workdir, "cdparanoia_out_*.log")
	if err != nil {
		panic(err)
	}

	cmd := exec.Command("cdparanoia", "-d", drive, "-A")
	cmd.Dir = status.Workdir
	cmd.Stderr = f
	cmd.Stdout = f

	if err := cmd.Start(); err != nil {
		panic(err)
	}
	status.Progress = 1
	jsonout.WriteStatus(status)
	if err := cmd.Wait(); err != nil {
		panic(err)
	}
	status.Progress = 99
	jsonout.WriteStatus(status)

	_ = f.Sync()
	_, _ = f.Seek(0, 0)

	scanner := bufio.NewScanner(f)

	r := regexp.MustCompile(`(?m)Drive tests OK with Paranoia\.`)

	for scanner.Scan() {
		if r.MatchString(scanner.Text()) {
			status.AnalyzeOK = true
		}
	}

	status.Progress = 100
	status.WorkDone = true
	jsonout.WriteStatus(status)
}

func byteToLSN(byte int) int {
	return byte / 44100 * 75 / 2
}

/**
RipDisc: `status` has to be prepared via cdinfo.DiscInfo first!
*/
func RipDisc(status *jsonout.StatusObject, drive string) {
	// pattern:			\[\S+\]\s@\s(\d+)
	// byte to LBA:		byte / 44100 * 75 /2

	if status.DiscInfo.LeadOutLSN == 0 {
		panic("cannot rip disc without leadout LSN")
	}

	status.RemoveDir = false

	cmd := exec.Command("cdparanoia", "-Bwed", drive)
	cmd.Dir = status.Workdir
	stderr, err := cmd.StderrPipe()
	if err != nil {
		panic(err)
	}
	cmd.Stdout = cmd.Stderr

	if err := cmd.Start(); err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(stderr)

	for scanner.Scan() {
		//fmt.Fprintf(os.Stderr, "line: %s\n", scanner.Text())
		var progress = regexp.MustCompile(`(?m)\[(?:read|wrote)\]\s@\s(\d+)`)
		for _, match := range progress.FindAllStringSubmatch(scanner.Text(), -1) {
			//fmt.Fprintf(os.Stderr, "match: %+v\n", match)
			if len(match) > 1 {
				bytesRead, err := strconv.Atoi(match[1])
				if err != nil {
					panic(err)
				}
				currentLBA := byteToLSN(bytesRead)
				progressPercent := int(float64(currentLBA) / float64(status.DiscInfo.LeadOutLSN) * float64(100))

				//fmt.Fprintf(os.Stderr, "debug: %d [%d/%d]\n", progressPercent, currentLBA, status.DiscInfo.LeadOutLSN)

				if progressPercent > int(status.Progress) {
					if progressPercent <= 99 {
						status.Progress = uint8(progressPercent)
						jsonout.WriteStatus(*status)
						fmt.Fprintf(os.Stderr, "progress: %d [%d/%d]\n", progressPercent, currentLBA, status.DiscInfo.LeadOutLSN)
					}
				}
			}
		}
	}

	if err := cmd.Wait(); err != nil {
		panic(err)
	}

	for k := range status.Tracks {
		if status.Tracks[k].Type == "audio" {
			f, err := os.Stat(path.Join(status.Workdir, fmt.Sprintf("track%02d.cdda.wav", status.Tracks[k].Number)))
			if err != nil {
				panic(err)
			}
			status.Tracks[k].FileName = path.Join(status.Workdir, f.Name())
		}
	}

	status.Progress = 100
	status.WorkDone = true
	jsonout.WriteStatus(*status)
}
