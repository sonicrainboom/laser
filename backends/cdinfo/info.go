package cdinfo

import (
	"bufio"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"gitlab.com/sonicrainboom/laser/jsonout"
	"io/ioutil"
	"os/exec"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

func DiscInfo(status *jsonout.StatusObject, drive string, reportProgress bool) {
	status.RemoveDir = true

	f, err := ioutil.TempFile(status.Workdir, "discinfo_*.log")
	if err != nil {
		panic(err)
	}

	cmd := exec.Command("cd-info", "--no-cddb", "-q", "--no-header", "-C", drive)
	cmd.Dir = status.Workdir
	cmd.Stderr = f
	cmd.Stdout = f

	if err := cmd.Start(); err != nil {
		panic(err)
	}
	if reportProgress {
		status.Progress = 1
		jsonout.WriteStatus(*status)
	}
	if err := cmd.Wait(); err != nil {
		panic(err)
	}
	if reportProgress {
		status.Progress = 99
		jsonout.WriteStatus(*status)
	}

	_ = f.Sync()
	_, _ = f.Seek(0, 0)

	scanner := bufio.NewScanner(f)

	tracks := map[string]jsonout.TrackObject{}

	cdtext_region := 0
	for scanner.Scan() {
		str := scanner.Text()

		//region CD-Text
		var cdtext_marker = regexp.MustCompile(`(?m)^CD-TEXT for (?:Track\s+)?(Disc|\d+):\s*$`)

		for _, match := range cdtext_marker.FindAllStringSubmatch(str, -1) {
			if len(match) > 1 {
				if match[1] == "Disc" {
					cdtext_region = 0
				} else {
					cdtext_region, err = strconv.Atoi(match[1])
					if err != nil {
						continue
					}
				}
			}
		}

		var cdtext_field = regexp.MustCompile(`(?m)^\s*(ARRANGER|COMPOSER|ISRC|MESSAGE|PERFORMER|SONGWRITER|TITLE|UPC_EAN):\s+(.+?)\s*$`)

		for _, match := range cdtext_field.FindAllStringSubmatch(str, -1) {
			if len(match) > 2 {
				if cdtext_region == 0 {
					// Disc
					switch strings.ToUpper(match[1]) {
					case "ARRANGER":
						status.DiscInfo.Arranger = match[2]
					case "COMPOSER":
						status.DiscInfo.Composer = match[2]
					case "MESSAGE":
						status.DiscInfo.Message = match[2]
					case "PERFORMER":
						status.DiscInfo.Performer = match[2]
					case "SONGWRITER":
						status.DiscInfo.Songwriter = match[2]
					case "TITLE":
						status.DiscInfo.Title = match[2]
					case "UPC_EAN":
						status.DiscInfo.Barcode = match[2]
					default:
						continue
					}

				} else {
					if _, ok := tracks[strconv.Itoa(cdtext_region)]; !ok {
						tracks[strconv.Itoa(cdtext_region)] = jsonout.TrackObject{}
					}
					tmp := tracks[strconv.Itoa(cdtext_region)]

					switch strings.ToUpper(match[1]) {
					case "ARRANGER":
						tmp.Arranger = match[2]
					case "COMPOSER":
						tmp.Composer = match[2]
					case "ISRC":
						tmp.ISRC = match[2]
					case "MESSAGE":
						tmp.Message = match[2]
					case "PERFORMER":
						tmp.Performer = match[2]
					case "SONGWRITER":
						tmp.Songwriter = match[2]
					case "TITLE":
						tmp.Title = match[2]
					case "UPC_EAN":
					default:
						continue
					}

					tracks[strconv.Itoa(cdtext_region)] = tmp
				}
			}
		}
		//endregion

		//region Track List
		var tracklist = regexp.MustCompile(`(?m)\s*(\d+):\s+(\d\d):(\d\d):(\d\d)\s+(\d+)\s+(\S+)(?:\s+(\S+)\s+(\S+)\s+(\d+)\s+(\S+))?`)

		for _, match := range tracklist.FindAllStringSubmatch(str, -1) {
			if len(match) > 6 {
				if _, ok := tracks[match[1]]; !ok {
					tracks[match[1]] = jsonout.TrackObject{}
				}
				tmp := tracks[match[1]]
				tmp.Number, err = strconv.Atoi(match[1])
				tmp.Len = -1
				if err != nil {
					panic(err)
				}
				{
					// MSF == MIN:SEC:sectors
					// 1 SEC == 75 sectors
					// Max vals: 99:59:74

					m, err := strconv.Atoi(match[2])
					if err != nil {
						panic(err)
					}
					s, err := strconv.Atoi(match[3])
					if err != nil {
						panic(err)
					}
					sec, err := strconv.Atoi(match[4])
					if err != nil {
						panic(err)
					}
					tmp.MSF = (((m * 60) + s) * 75) + sec
				}
				tmp.LSN, err = strconv.Atoi(match[5])
				if err != nil {
					panic(err)
				}
				tmp.Type = match[6]
				if match[6] == "audio" {
					if len(match) > 10 {
						tmp.Channels, err = strconv.Atoi(match[9])
						if err != nil {
							panic(err)
						}
						if match[7] == "true" || match[7] == "yes" {
							tmp.Green = true
						}
						if match[8] == "true" || match[8] == "yes" {
							tmp.Copy = true
						}
						if match[10] == "true" || match[10] == "yes" {
							tmp.Premphasis = true
						}
					} else {
						panic("invalid format")
					}
				}
				tracks[match[1]] = tmp
			}
		}
		//endregion

		//region ISRC
		var re = regexp.MustCompile(`(?m)TRACK\s+(\d+)(?:.+ISRC:\s(\S+))?`)
		for _, match := range re.FindAllStringSubmatch(str, -1) {
			if len(match) > 2 {
				if _, ok := tracks[match[1]]; !ok {
					tracks[match[1]] = jsonout.TrackObject{}
				}
				tmp := tracks[match[1]]
				tmp.Number, err = strconv.Atoi(match[1])
				if err != nil {
					panic(err)
				}

				// We have an ISRC :)
				if len(match) == 3 {
					tmp.ISRC = match[2]
				}
				tracks[match[1]] = tmp
			} else {
				// No TRACK line here
				continue
			}
		}

		//endregion
	}

	var discOffset int
	status.Tracks, discOffset = prepareTracks(tracks)
	status.DiscInfo.LSNToLBAOffset = discOffset
	status.DiscID, status.DiscInfo.LeadOutLSN = CalcDiscID(status.Tracks, discOffset)
	if reportProgress {
		status.Progress = 100
		status.WorkDone = true
		jsonout.WriteStatus(*status)
	}
}

const XA_INTERVAL = (60 + 90 + 2) * 75

/**
Validated against https://musicbrainz.org/doc/Disc_ID_Calculation
*/
func prepareTracks(trackMap map[string]jsonout.TrackObject) (tracks []jsonout.TrackObject, discOffset int) {
	for _, track := range trackMap {
		tracks = append(tracks, track)
	}

	sort.SliceStable(tracks, func(a, b int) bool {
		return tracks[a].Number < tracks[b].Number
	})

	discOffset = 150
	lastAudioTrack := -1
	firstAudioTrack := -1
	for i := range tracks {
		if i == 0 {
			discOffset = tracks[i].MSF
		}
		if tracks[i].Type == "audio" {
			lastAudioTrack = i
			if firstAudioTrack < 0 {
				firstAudioTrack = i
			}
		}
	}

	// disc->track_offsets[i] = track[i] + discOffset

	for i := range tracks {
		if i == 0 {
			discOffset = tracks[i].MSF
		} else {
			// Is there another track between the last audio track and this one? (`this` might be the leadout)
			if lastAudioTrack < i-1 {
				// Shortcut the `leadout` to be the last track +1 minus one leadout length (XA_INTERVAL)
				tracks[lastAudioTrack+1].LSN -= XA_INTERVAL
				// We also need to shorten the length, as that would have included the leadout
				tracks[lastAudioTrack].Len -= XA_INTERVAL
			} else if lastAudioTrack < i {
				// leadout or equivalent, override
				tracks[i].Type = "leadout"
			}
			tracks[i-1].Len = tracks[i].LSN - tracks[i-1].LSN
		}
	}

	return
}

/**
Validated against https://musicbrainz.org/doc/Disc_ID_Calculation
This assumes `tracks` is sorted by track number
*/
func CalcDiscID(tracks []jsonout.TrackObject, discOffset int) (discID jsonout.DiscIDObj, leadOutLBA int) {

	var audioTracksHex []string
	var audioTracks []string
	var leadOut int
	var first, last int
	for i := range tracks {
		if tracks[i].Type == "audio" {
			if first < 1 {
				first = tracks[i].Number
			}
			last = tracks[i].Number
			audioTracksHex = append(audioTracksHex, fmt.Sprintf("%08X", tracks[i].LSN+discOffset))
			audioTracks = append(audioTracks, fmt.Sprintf("%d", tracks[i].LSN+discOffset))
		}
		if tracks[i].Type == "leadout" {
			leadOutLBA = tracks[i].LSN
			leadOut = tracks[i].LSN + discOffset
			// Only the 1st leadout is read
			break
		}
	}

	toc := fmt.Sprintf("%d %d %d %s", first, last, leadOut, strings.Join(audioTracks, " "))

	sum := sha1.New()
	sum.Write([]byte(fmt.Sprintf("%02X", first)))
	sum.Write([]byte(fmt.Sprintf("%02X", last)))
	sum.Write([]byte(fmt.Sprintf("%08X", leadOut)))

	written := 0

	for i := range tracks {
		if tracks[i].Type == "audio" {
			sum.Write([]byte(fmt.Sprintf("%08X", tracks[i].LSN+discOffset)))
			written++
		}
	}

	for written < 99 {
		sum.Write([]byte(fmt.Sprintf("%08X", 0)))
		written++
	}

	h := sum.Sum(nil)
	mbID := base64.StdEncoding.EncodeToString(h)
	mbID = strings.ReplaceAll(mbID, "+", ".")
	mbID = strings.ReplaceAll(mbID, "/", "_")
	mbID = strings.ReplaceAll(mbID, "=", "-")

	discID.TOC = toc
	discID.MusicBrainzID = mbID

	return
}
