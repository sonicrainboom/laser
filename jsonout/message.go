package jsonout

import (
	"encoding/json"
	"os"
	"strings"
)

type errObj struct {
	Error   string `json:"error"`
	Message string `json:"message,omitempty"`
}

type TrackObject struct {
	// CD-TEXT fields:
	ISRC       string `json:"isrc,omitempty"`
	Arranger   string `json:"arranger,omitempty"`
	Composer   string `json:"composer,omitempty"`
	Message    string `json:"message,omitempty"`
	Performer  string `json:"performer,omitempty"`
	Songwriter string `json:"songwriter,omitempty"`
	Title      string `json:"title,omitempty"`

	// Other fields:
	Type       string `json:"type"`
	Number     int    `json:"number"`
	LSN        int    `json:"lsn"`
	MSF        int    `json:"msf"`
	Len        int    `json:"len"` // -1 == unknown, otherwise length in sectors
	Channels   int    `json:"channels"`
	Green      bool   `json:"green"`
	Copy       bool   `json:"copy"`
	Premphasis bool   `json:"premphasis"`
	FileName   string `json:"file_name,omitempty"`
}

type DiscInfo struct {
	// CD-TEXT fields:
	Arranger   string `json:"arranger,omitempty"`
	Composer   string `json:"composer,omitempty"`
	Message    string `json:"message,omitempty"`
	Performer  string `json:"performer,omitempty"`
	Songwriter string `json:"songwriter,omitempty"`
	Title      string `json:"title,omitempty"`
	Barcode    string `json:"barcode,omitempty"`

	LSNToLBAOffset int `json:"lsn_to_lba_offset"`
	LeadOutLSN     int `json:"lead_out_lsn"`
}
type DiscIDObj struct {
	TOC           string `json:"toc"`
	MusicBrainzID string `json:"music_brainz_id"`
}

type StatusObject struct {
	Workdir   string        `json:"workdir"`
	WorkType  string        `json:"work_type"`
	RemoveDir bool          `json:"remove_dir"`
	Progress  uint8         `json:"progress,omitempty"` // 0-100
	WorkDone  bool          `json:"work_done"`
	AnalyzeOK bool          `json:"analyze_ok,omitempty"` // if a `cdparanoia -A` contains 'Drive tests OK with Paranoia.'
	Tracks    []TrackObject `json:"tracks,omitempty"`
	DiscID    DiscIDObj     `json:"disc_id,omitempty"`
	DiscInfo  DiscInfo      `json:"disc_info,omitempty"`
}

func WriteError(err error, message ...string) {
	e := errObj{Error: err.Error(), Message: strings.Join(message, "\n")}
	j := json.NewEncoder(os.Stdout)
	_ = j.Encode(e)
	_ = os.Stderr.Sync()
}

func WriteStatus(status StatusObject) {
	j := json.NewEncoder(os.Stdout)
	_ = j.Encode(status)
	_ = os.Stderr.Sync()
}
